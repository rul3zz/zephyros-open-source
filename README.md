# Zephyros - Open-Source Theme

The first open source theme by Zephyros for Multi-Theft-Auto. The theme is easily customizable and beginner friendly

## Getting Started

These instructions will get you a copy of the project up and running on your Community-Website. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Invision Community Version 4.6
Running Webserver Nginx or Apache2
An Database for your Community
PHP 7.4 or newer
```

### Installing

A step by step series of examples that tell you how to get the theme running

Step 1 | Activate Designer Mode

```
Go to your Admin-CP > Themes > Designer Mode
```

Step 2 | Create an new Theme
```
Create a new Theme in your AdminCP and select "Manual Mode"
```


Step 3 | Upload Files

```
Go to your Webserver FTP and under Themes/ID "select your Theme ID which you created" upload every file contains in the Repository
```

Step 4 | Apply the Theme in your Forum

```
Now simply disable the Designer Mode and sync every changes and also delete Local Files. Enjoy it :)
```

## Authors

* **Andreas Schäfer** - *Initial work* - [Rul3zZ](https://gitlab.com/rul3zz)

See also [Zephyros](https://zephyro-s.com) who own this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details


